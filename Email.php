<?php
declare(strict_types=1);

final class Email
{
    private $email;

    private function __construct(string $email)
    {
        $this->ensureIsValidEmail($email);

        $this->email = $email;
    }

    public static function fromString(string $email): self
    {
        return new self($email);
    }

    public function __toString(): string
    {
        return $this->email;
    }

    private function ensureIsValidEmail(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a valid email address',
                    $email
                )
            );
        }
    }
    
    
    public function fromPAD(string $email)
    {
    $parts = explode('@', $email);
        if (isset($parts[1]) && $parts[1]!=='portdakar.sn') {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a PAD valid email address',
                    $email
                )
            );
        }
        return True;
    }
}