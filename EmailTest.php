<?php
//declare(strict_types=1);

use PHPUnit\Framework\TestCase;
include_once("Email.php");

final class EmailTest extends TestCase
{
    public function testCanBeCreatedFromValidEmailAddress()
    {
        $this->assertInstanceOf(
            Email::class,
            Email::fromString('user@example.com')
        );
    }

    public function testAdresseIsFromPAD()
    {
        $this->assertEquals(
            True,
            Email::fromPAD('user@portdakar.sn')
        );
    }

    public function testCanBeUsedAsString()
    {
        $this->assertEquals(
            'user@example.com',
            Email::fromString('user@example.com')
        );
    }
}